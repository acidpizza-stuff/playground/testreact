import { MantineProvider, Text } from '@mantine/core';
import { HeaderSimple } from './components/header/header';

const headerLinks = [
  {
    label: "Google",
    link: "https://google.com",
  }
]

export default function App() {
  return (
    <MantineProvider withGlobalStyles withNormalizeCSS>
      <HeaderSimple links={headerLinks} />
      <Text>Welcome to Mantine!</Text>
    </MantineProvider>
  );
}